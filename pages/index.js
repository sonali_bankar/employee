import styles from '../styles/Home.module.css'
import Title from "../components/Title"
export default function Home({ data }) {
  console.log(data)
  return (
    <>
      <Title title="Employee Home Page" />
      <div className="md:w-full sm:1/2 m-auto font-serif mb-5">
        <h1 className="mt-16 font-bold md:text-3xl sm:text-2xl text-center">Employees Data</h1>
        <div className="p-16 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3  gap-5 mt-3">
          {
            data.map((item, i) => (
              <div className="rounded overflow-hidden shadow bg-gray-50" key={i}>
                <div className="px-6 pt-4 pb-5">
                  <h4 className="font-bold pb-1 pt-2 md:text-xl sm:text-l text-center">{item.name}</h4>

                  <p className=" pt-5   text-center"><strong>Email :</strong> {item.email}</p>
                  <p className=" pt-5 text-center"><strong>Address :</strong> {item.address.street} - {item.address.zipcode}</p>
                  <p className=" pt-5 text-center"><strong>City : </strong>{item.address.city}</p>
                  <p className=" pt-5 text-center"><strong>Phone_No : </strong>{item.phone}</p>
                </div>
              </div>
            ))
          }
        </div>
      </div>
    </>
  )
}


export async function getStaticProps() {
  const res = await fetch("https://jsonplaceholder.typicode.com/users")
  const data = await res.json()
  return {
    props: {
      data
    }
  }
}
